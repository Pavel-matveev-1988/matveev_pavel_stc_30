public class Program {

    public static void main(String[] args) {

        int[] array = new int[]{4, 6, 10, 14, 18, 22, 24, 32, 46};
        int elementToSearch = 10;
        int firstElement = 0;
        int index = binarySearch(array, firstElement, array.length, elementToSearch);
        System.out.println("index for " + elementToSearch + " = " + index);
    }

    public static int binarySearch(int arr[], int firstElement, int lastElement, int elementToSearch) {
        if (lastElement >= firstElement) {
            int mid = (firstElement + lastElement) / 2;
            System.out.println(mid);

            if (arr[mid] == elementToSearch) {
                return mid;
            }
            if (arr[mid] > elementToSearch) {
                return binarySearch(arr, firstElement, mid - 1, elementToSearch);
            }
                return binarySearch(arr, mid + 1, lastElement, elementToSearch);
            }
            return -1;
        }

}

